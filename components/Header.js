import React from 'react'
import Link from 'next/link'

class Header extends React.Component{
    render(){
        return(
            <header className="header">
			{/* <!-- Topbar --> */}
			<div className="topbar">
				<div className="container">
					<div className="row">
						<div className="col-lg-8 col-12">
							{/* <!-- Top Contact --> */}
							<div className="top-contact">
								<div className="single-contact"><i className="fa fa-phone"></i>Phone: +(600) 125-4985-214</div> 
								<div className="single-contact"><i className="fa fa-envelope-open"></i>Email: info@yoursite.com</div>	
								<div className="single-contact"><i className="fa fa-clock-o"></i>Opening: 08AM - 09PM</div> 
							</div>
							{/* <!-- End Top Contact --> */}
						</div>	
						<div className="col-lg-4 col-12">
							<div className="topbar-right">
								{/* <!-- Social Icons --> */}
								<ul className="social-icons">
									<li><a href="#"><i className="fa fa-facebook"></i></a></li>
									<li><a href="#"><i className="fa fa-twitter"></i></a></li>
									<li><a href="#"><i className="fa fa-linkedin"></i></a></li>
									<li><a href="#"><i className="fa fa-dribbble"></i></a></li>
								</ul>															
								<div className="button">
									<a href="contact.html" className="bizwheel-btn">Live Streaming</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			{/* <!--/ End Topbar -->
			<!-- Middle Header --> */}
			<div className="middle-header">
				<div className="container">
					<div className="row">
						<div className="col-12">
							<div className="middle-inner">
								<div className="row">
									<div className="col-lg-2 col-md-3 col-12" style={{ alignSelf:'center' }}>
										{/* <!-- Logo --> */}
										<div className="logo">
											{/* <!-- Image Logo --> */}
											<div>
												<a href="index.html">
													<img src="/static/img/logoffi.png" alt="#" />
												</a>
											</div>
										</div>								
										<div className="mobile-nav"></div>
									</div>
									<div className="col-lg-10 col-md-9 col-12">
										<div className="menu-area">
											{/* <!-- Main Menu --> */}
											<nav className="navbar navbar-expand-lg">
												<div className="navbar-collapse">	
													<div className="nav-inner">	
														<div className="menu-home-menu-container">
															{/* <!-- Naviagiton --> */}
															<ul id="nav" className="nav main-menu menu navbar-nav">
																<li><Link href={{ 
                                                                    pathname: '/',
                                                                 }}><a>Beranda</a></Link></li>
																<li><a href="blog.html">Kabar Futsal</a></li> 
																<li className="icon-active"><a href="#">Tentang AFP Jatim</a>
																	<ul className="sub-menu">
																		<li><Link href={{ 
                                                                    pathname: '/sejarah',
                                                                 }}><a>Sejarah</a></Link></li>
																		<li><a href="struktur.html">Struktur Organisasi</a></li>
																		<li><a href="#">Anggota</a>
																			<ul className="sub-menu">
																				<li><a href="afkab-afkot.html">AFKAB/AFKOT</a></li>
																				<li><a href="klub.html">Klub</a></li>
																			</ul>
																		</li>
																		<li><a href="contact.html">Kontak</a></li>
																	</ul>
																</li>
																<li className="icon-active"><a href="#">Program</a>
																	<ul className="sub-menu">
																		<li><a href="kompetisi.html">Kompetisi</a></li> 
																		<li><a href="blog-single.html">Pengembangan</a></li>
																	</ul>
																</li>
															</ul>
															{/* <!--/ End Naviagiton --> */}
														</div>
													</div>
												</div>
											</nav>
											{/* <!--/ End Main Menu -->	
											<!-- Right Bar --> */}
											<div className="right-bar">
												{/* <!-- Search Bar --> */}
												<ul className="right-nav">
													<li className="top-search"><a href="#0"><i className="fa fa-search"></i></a></li>
													<li className="bar"><a className="fa fa-bars"></a></li>
												</ul>
												{/* <!--/ End Search Bar -->
												<!-- Search Form --> */}
												<div className="search-top">
													<form action="#" className="search-form" method="get">
														<input type="text" name="s" placeholder="Search here"/>
														<button type="submit" id="searchsubmit"><i className="fa fa-search"></i></button>
													</form>
												</div>
												{/* <!--/ End Search Form --> */}
											</div>	
											{/* <!--/ End Right Bar --> */}
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			{/* <!--/ End Middle Header -->
			<!-- Sidebar Popup --> */}
			<div className="sidebar-popup">
				<div className="cross">
					<a className="btn"><i className="fa fa-close"></i></a>
				</div>
				<div className="single-content">
					<h4>About Bizwheel</h4>
					<p>The main component of a healthy environment for self esteem is that it needs be nurturing. It should provide unconditional warmth.</p>
					{/* <!-- Social Icons --> */}
					<ul className="social">
						<li><a href="#"><i className="fa fa-facebook"></i></a></li>
						<li><a href="#"><i className="fa fa-twitter"></i></a></li>
						<li><a href="#"><i className="fa fa-linkedin"></i></a></li>
						<li><a href="#"><i className="fa fa-dribbble"></i></a></li>
					</ul>
				</div>
				<div className="single-content">
					<h4>Important Links</h4>   
					<ul className="links">
						<li><a href="#">About Us</a></li>
						<li><a href="#">Kabar Futsal</a></li>
						<li><a href="#">Portfolio</a></li>
						<li><a href="#">Pricing Plan</a></li>
						<li><a href="#">Blog & News</a></li>
						<li><a href="#">Contact us</a></li>
					</ul>
				</div>	
			</div>
			{/* <!--/ Sidebar Popup -->	 */}
		    </header>
        )
    }
}

export default Header