import React from 'react'
import dynamic from 'next/dynamic'

const Header = dynamic(() => import('./Header'))
const Slider = dynamic(() => import('./Slider'))
const Footer = dynamic(() => import('./Footer'))

class Layout extends React.Component{
    render(){
        return(
            <div id="page" className="site boxed-layout"> 
		
            {/* <!-- Preloader --> */}
            {/* <div className="preeloader">
                <div className="preloader-spinner"></div>
            </div> */}
                <Header />
                <Slider />
                {this.props.children}
                <Footer />
            </div>
        );
    }
}

export default Layout