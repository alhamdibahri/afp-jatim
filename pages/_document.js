import Document, {Head, Main, NextScript } from 'next/document';
import flush from 'styled-jsx/server';

export default class MyDocument extends Document {
//   static getInitialProps({ renderPage }) {
//     const { html, head, errorHtml, chunks } = renderPage();
//     const styles = flush();
    
//     return { html, head, errorHtml, chunks, styles };
//   }

  render() {
    return (
      <html>
        <Head>
        <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@300;400;500;600;700&display=swap" rel="stylesheet" />
		
		<link rel="stylesheet" href="/static/css/animate.min.css" />
		<link rel="stylesheet" href="/static/css/bootstrap.min.css"/>
		<link rel="stylesheet" href="/static/css/cubeportfolio.min.css"/>
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/css/all.min.css" integrity="sha512-+4zCK9k+qNFUR5X+cKL9EIR+ZOhtIloNl9GIKS57V1MyNsYpYcUrUeQc9vNfzsWfV28IaLL3i96P9sdNyeRssA==" crossorigin="anonymous" />
		<link rel="stylesheet" href="/static/css/jquery.fancybox.min.css" />
		<link rel="stylesheet" href="/static/css/magnific-popup.min.css" />
		<link rel="stylesheet" href="/static/css/owl-carousel.min.css" />
		<link rel="stylesheet" href="/static/css/slicknav.min.css" />

		<link rel="stylesheet" href="/static/css/reset.css" />
		<link rel="stylesheet" href="/static/style.css" />
		<link rel="stylesheet" href="/static/css/responsive.css" />

        </Head>
        <body id="bg">
          <Main />
          <NextScript />

			<script src="/static/js/jquery.min.js"></script>
			<script src="/static/js/jquery-migrate-3.0.0.js"></script>
			<script src="/static/js/popper.min.js"></script>
			<script src="/static/js/bootstrap.min.js"></script>
			<script src="/static/js/modernizr.min.js"></script>
			<script src="/static/js/scrollup.js"></script>
			<script src="/static/js/jquery-fancybox.min.js"></script>
			<script src="/static/js/cubeportfolio.min.js"></script>
			<script src="/static/js/slicknav.min.js"></script>
			<script src="/static/js/slicknav.min.js"></script>
			<script src="/static/js/owl-carousel.min.js"></script>
			<script src="/static/js/easing.js"></script>
			<script src="/static/js/magnific-popup.min.js"></script>
			<script src="/static/js/active.js"></script>
			<script src="https://cdnjs.cloudflare.com/ajax/libs/Counter-Up/1.0.0/jquery.counterup.min.js" integrity="sha512-d8F1J2kyiRowBB/8/pAWsqUl0wSEOkG5KATkVV4slfblq9VRQ6MyDZVxWl2tWd+mPhuCbpTB4M7uU/x9FlgQ9Q==" crossorigin="anonymous"></script>
        </body>
      </html>
    );
  }
}