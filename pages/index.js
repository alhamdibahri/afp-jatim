import Link from 'next/link';
import Head from 'next/head';
import styles from '../styles/Home.module.css';
import React from 'react'
import Layout from '../components/Layout'

class Home extends React.Component{
	render(){
		return(
			<div>
				<section className="features-area section-bg">
			<div className="container">
				<div className="row">
					<div className="col-lg-3 col-md-6 col-12">
						{/* <!-- Single Feature --> */}
						<div className="single-feature">
							<div className="icon-head"><i className="fa fa-podcast"></i></div>
							<h4><a href="service-single.html">Creative Design</a></h4>
							<p>Aenean aliquet rutrum enimn scelerisque. Nam dictumanpo, antequis laoreet ullamcorper, velitsd odio scelerisque tod</p>
							<div className="button">
								<a href="service-single.html" className="bizwheel-btn"><i className="fa fa-arrow-circle-o-right"></i>Learn More</a>
							</div>
						</div>
						{/* <!--/ End Single Feature --> */}
					</div>
					<div className="col-lg-3 col-md-6 col-12">
						{/* <!-- Single Feature --> */}
						<div className="single-feature">
							<div className="icon-head"><i className="fa fa-podcast"></i></div>
							<h4><a href="service-single.html">Quality Service</a></h4>
							<p>Aenean aliquet rutrum enimn scelerisque. Nam dictumanpo, antequis laoreet ullamcorper, velitsd odio scelerisque tod</p>
							<div className="button">
								<a href="service-single.html" className="bizwheel-btn"><i className="fa fa-arrow-circle-o-right"></i>Learn More</a>
							</div>
						</div>
						{/* <!--/ End Single Feature --> */}
					</div>
					<div className="col-lg-3 col-md-6 col-12">
						{/* <!-- Single Feature --> */}
						<div className="single-feature active">
							<div className="icon-head"><i className="fa fa-podcast"></i></div>
							<h4><a href="service-single.html">On-time Delivery</a></h4>
							<p>Aenean aliquet rutrum enimn scelerisque. Nam dictumanpo, antequis laoreet ullamcorper, velitsd odio scelerisque tod</p>
							<div className="button">
								<a href="service-single.html" className="bizwheel-btn"><i className="fa fa-arrow-circle-o-right"></i>Learn More</a>
							</div>
						</div>
						{/* <!--/ End Single Feature --> */}
					</div>
					<div className="col-lg-3 col-md-6 col-12">
						{/* <!-- Single Feature --> */}
						<div className="single-feature">
							<div className="icon-head"><i className="fa fa-podcast"></i></div>
							<h4><a href="service-single.html">24/7 Live support</a></h4>
							<p>Aenean aliquet rutrum enimn scelerisque. Nam dictumanpo, antequis laoreet ullamcorper, velitsd odio scelerisque tod</p>
							<div className="button">
								<a href="service-single.html" className="bizwheel-btn"><i className="fa fa-arrow-circle-o-right"></i>Learn More</a>
							</div>
						</div>
						{/* <!--/ End Single Feature --> */}
					</div>
				</div>
			</div>
		</section>
		{/* <!--/ End Features Area -->
		
		<!-- Call To Action --> */}
		<section className="call-action overlay" style={{backgroundImage: "url('https://via.placeholder.com/1700x800.png')"}}>
			<div className="container">
				<div className="row">
					<div className="col-lg-9 col-12">
						<div className="call-inner">
							<h2>Brand Products &amp; Creativity is our Fashion</h2>
							<p>ehicula maximus velit. Morbi non tincidunt purus, a hendrerit nisi. Vivamus elementum</p>
						</div>
					</div>
					<div className="col-lg-3 col-12">
						<div className="button">
							<a href="portfolio.html" className="bizwheel-btn">Program</a>
						</div>
					</div>
				</div>
			</div>
		</section>
		{/* <!--/ End Call to action -->
		
		<!-- Services --> */}
		<section className="services section-bg section-space">
			<div className="container">
				<div className="row">
					<div className="col-12">
						<div className="section-title style2 text-center">
							<div className="section-top">
								<h1><span>Creative</span><b>Service We Provide</b></h1><h4>We provide quality service &amp; support..</h4>
							</div>
							<div className="section-bottom">
								<div className="text-style-two">
									<p>Aliquam Sodales Justo Sit Amet Urna Auctor Scelerisquinterdum Leo Anet Tempus Enim Esent Egetis Hendrerit Vel Nibh Vitae Ornar Sem Velit Aliquam</p>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div className="row">
					<div className="col-lg-4 col-md-4 col-12">
						{/* <!-- Single Service --> */}
						<div className="single-service">
							<div className="service-head">
								<img src="https://via.placeholder.com/555x410" />
								<div className="icon-bg"><i className="fa fa-handshake-o"></i></div>
							</div>
							<div className="service-content">
								<h4><a href="service-business.html">Business Strategy</a></h4>
								<p>Cras venenatis, purus sit amet tempus mattis, justo nisi facilisis metus, in tempus ipsum ipsum eu ipsum. className aptent taciti</p>
								<a className="btn" href="service-business.html"><i className="fa fa-arrow-circle-o-right"></i>View Service</a>
							</div>
						</div>
						{/* <!--/ End Single Service --> */}
					</div>
					<div className="col-lg-4 col-md-4 col-12">
						{/* <!-- Single Service --> */}
						<div className="single-service">
							<div className="service-head">
								<img src="https://via.placeholder.com/555x410" />
								<div className="icon-bg"><i className="fa fa-html5"></i></div>
							</div>
							<div className="service-content">
								<h4><a href="service-develop.html">Web Development</a></h4>
								<p>Cras venenatis, purus sit amet tempus mattis, justo nisi facilisis metus, in tempus ipsum ipsum eu ipsum. className aptent taciti</p>
								<a className="btn" href="service-develop.html"><i className="fa fa-arrow-circle-o-right"></i>View Service</a>
							</div>
						</div>
						{/* <!--/ End Single Service --> */}
					</div>
					<div className="col-lg-4 col-md-4 col-12">
						{/* <!-- Single Service --> */}
						<div className="single-service">
							<div className="service-head">
								<img src="https://via.placeholder.com/555x410"/>
								<div className="icon-bg"><i className="fa fa-cube"></i></div>
							</div>
							<div className="service-content">
								<h4><a href="service-market.html">Market Research</a></h4>
								<p>Cras venenatis, purus sit amet tempus mattis, justo nisi facilisis metus, in tempus ipsum ipsum eu ipsum. className aptent taciti</p>
								<a className="btn" href="service-market.html"><i className="fa fa-arrow-circle-o-right"></i>View Service</a>
							</div>
						</div>
						{/* <!--/ End Single Service --> */}
					</div>
				</div>
			</div>
		</section>
		{/* <!--/ End Services --> */}
		
	
		
		{/* <!-- Testimonials --> */}
		<section className="testimonials section-space" style={{backgroundImage: "url('https://via.placeholder.com/1700x800.png')"}}>
			<div className="container">
				<div className="row">
					<div className="col-lg-6 col-md-9 col-12">
						<div className="section-title default text-left">
							<div className="section-top">
								<h1><b>Our Satisfied Clients</b></h1>
							</div>
							<div className="section-bottom">
								<div className="text"><p>some of our great clients and their review</p></div>
							</div>
						</div>
						<div className="testimonial-inner">
							<div className="testimonial-slider">
								{/* <!-- Single Testimonial --> */}
								<div className="single-slider">
									<ul className="star-list">
										<li><i className="fa fa-star"></i></li>
										<li><i className="fa fa-star"></i></li>
										<li><i className="fa fa-star"></i></li>
										<li><i className="fa fa-star"></i></li>
										<li><i className="fa fa-star"></i></li>
									</ul>
									<p>simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the <strong>industry's standard</strong> dummy text ever sinsimply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard</p>
									{/* <!-- Client Info --> */}
									<div className="t-info">
										<div className="t-left">
											<div className="client-head"><img src="https://via.placeholder.com/70x70" alt="#" /></div>
											<h2>Julias Dien <span>CEO / Creative IT</span></h2>
										</div>
										<div className="t-right">
											<div className="quote"><i className="fa fa-quote-right"></i></div>
										</div>
									</div>
								</div>
								{/* <!-- / End Single Testimonial -->
								<!-- Single Testimonial --> */}
								<div className="single-slider">
									<ul className="star-list">
										<li><i className="fa fa-star"></i></li>
										<li><i className="fa fa-star"></i></li>
										<li><i className="fa fa-star"></i></li>
										<li><i className="fa fa-star"></i></li>
										<li><i className="fa fa-star"></i></li>
									</ul>
									<p>simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the <strong>industry's standard</strong> dummy text ever sinsimply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard</p>
									{/* <!-- Client Info --> */}
									<div className="t-info">
										<div className="t-left">
											<div className="client-head"><img src="https://via.placeholder.com/70x70" alt="#" /></div>
											<h2>Buman Panama <span>Founder, Jolace Group</span></h2>
										</div>
										<div className="t-right">
											<div className="quote"><i className="fa fa-quote-right"></i></div>
										</div>
									</div>
								</div>
								{/* <!-- / End Single Testimonial -->
								<!-- Single Testimonial --> */}
								<div className="single-slider">
									<ul className="star-list">
										<li><i className="fa fa-star"></i></li>
										<li><i className="fa fa-star"></i></li>
										<li><i className="fa fa-star"></i></li>
										<li><i className="fa fa-star"></i></li>
										<li><i className="fa fa-star"></i></li>
									</ul>
									<p>simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the <strong>industry's standard</strong> dummy text ever sinsimply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard</p>	
									{/* <!-- Client Info --> */}
									<div className="t-info">
										<div className="t-left">
											<div className="client-head"><img src="https://via.placeholder.com/70x70" alt="#" /></div>
											<h2>Donald Bonam <span>Developer, Soft IT</span></h2>
										</div>
										<div className="t-right">
											<div className="quote"><i className="fa fa-quote-right"></i></div>
										</div>
									</div>
								</div>
								{/* <!-- / End Single Testimonial --> */}
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
		{/* <!--/ End Testimonials -->
		
		<!-- Latest Blog --> */}
		<section className="latest-blog section-space">
			<div className="container">
				<div className="row">
					<div className="col-lg-6 offset-lg-3 col-md-8 offset-md-2 col-12">
						<div className="section-title default text-center">
							<div className="section-top">
								<h1><span>Latest</span><b> Published</b></h1>
							</div>
							<div className="section-bottom">
								<div className="text">
									<p>Lorem Ipsum Dolor Sit Amet, Conse Ctetur Adipiscing Elit, Sed Do Eiusmod Tempor Ares Incididunt Utlabore. Dolore Magna Ones Baliqua</p>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div className="row">
					<div className="col-12">
						<div className="blog-latest blog-latest-slider">
							<div className="single-slider">
								{/* <!-- Single Blog --> */}
								<div className="single-news ">
									<div className="news-head overlay">
										<span className="news-img" style={{ backgroundImage: "url('https://via.placeholder.com/700x530')" }}></span>
										<a href="#" className="bizwheel-btn theme-2">Read more</a>
									</div>
									<div className="news-body">
										<div className="news-content">
											<h3 className="news-title"><a href="blog-single.html">We Provide you Best &amp; Creative Consulting Service</a></h3>
											<div className="news-text"><p>Sed tempus pulvinar augue ut euismod. Donec a nisi volutpat, dignissim mauris eget. Quisque vitae nunc sit amet eros pellentesque tempus at sit amet sem. Maecenas feugiat mauris</p></div>
											<ul className="news-meta">
												<li className="date"><i className="fa fa-calendar"></i>April 2020</li>
												<li className="view"><i className="fa fa-comments"></i>0</li>
											</ul>
										</div>
									</div>
								</div>
								{/* <!--/ End Single Blog --> */}
							</div>
							<div className="single-slider">
								{/* <!-- Single Blog --> */}
								<div className="single-news ">
									<div className="news-head overlay">
										<span className="news-img" style={{ backgroundImage: "url('https://via.placeholder.com/700x530')" }}></span>
										<a href="#" className="bizwheel-btn theme-2">Read more</a>
									</div>
									<div className="news-body">
										<div className="news-content">
											<h3 className="news-title"><a href="blog-single.html">We Provide you Best &amp; Creative Consulting Service</a></h3>
											<div className="news-text"><p>Sed tempus pulvinar augue ut euismod. Donec a nisi volutpat, dignissim mauris eget. Quisque vitae nunc sit amet eros pellentesque tempus at sit amet sem. Maecenas feugiat mauris</p></div>
											<ul className="news-meta">
												<li className="date"><i className="fa fa-calendar"></i>April 2020</li>
												<li className="view"><i className="fa fa-comments"></i>0</li>
											</ul>
										</div>
									</div>
								</div>
								{/* <!--/ End Single Blog --> */}
							</div>
							<div className="single-slider">
								{/* <!-- Single Blog --> */}
								<div className="single-news ">
									<div className="news-head overlay">
										<span className="news-img" style={{ backgroundImage: "url('https://via.placeholder.com/700x530')" }}></span>
										<a href="#" className="bizwheel-btn theme-2">Read more</a>
									</div>
									<div className="news-body">
										<div className="news-content">
											<h3 className="news-title"><a href="blog-single.html">We Provide you Best &amp; Creative Consulting Service</a></h3>
											<div className="news-text"><p>Sed tempus pulvinar augue ut euismod. Donec a nisi volutpat, dignissim mauris eget. Quisque vitae nunc sit amet eros pellentesque tempus at sit amet sem. Maecenas feugiat mauris</p></div>
											<ul className="news-meta">
												<li className="date"><i className="fa fa-calendar"></i>April 2020</li>
												<li className="view"><i className="fa fa-comments"></i>0</li>
											</ul>
										</div>
									</div>
								</div>
								{/* <!--/ End Single Blog --> */}
							</div>
							<div className="single-slider">
								{/* <!-- Single Blog --> */}
								<div className="single-news ">
									<div className="news-head overlay">
										<span className="news-img" style={{backgroundImage: "url('https://via.placeholder.com/1700x800.png')"}}></span>
										<a href="#" className="bizwheel-btn theme-2">Read more</a>
									</div>
									<div className="news-body">
										<div className="news-content">
											<h3 className="news-title"><a href="blog-single.html">We Provide you Best &amp; Creative Consulting Service</a></h3>
											<div className="news-text"><p>Sed tempus pulvinar augue ut euismod. Donec a nisi volutpat, dignissim mauris eget. Quisque vitae nunc sit amet eros pellentesque tempus at sit amet sem. Maecenas feugiat mauris</p></div>
											<ul className="news-meta">
												<li className="date"><i className="fa fa-calendar"></i>April 2020</li>
												<li className="view"><i className="fa fa-comments"></i>0</li>
											</ul>
										</div>
									</div>
								</div>
								{/* <!--/ End Single Blog --> */}
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
		{/* <!--/ End Latest Blog -->
		
		<!-- Client Area --> */}
		<div className="clients section-bg">
			<div className="container">
				<div className="row">
					<div className="col-12">
						<div className="partner-slider">
							{/* <!-- Single client --> */}
							<div className="single-slider">
								<div className="single-client">
									<a href="#" target="_blank"><img src="/static/img/client/client-1.png" alt="#" /></a>
								</div>
							</div>
							{/* <!--/ End Single client -->
							<!-- Single client --> */}
							<div className="single-slider">
								<div className="single-client">
									<a href="#" target="_blank"><img src="/static/img/client/client-2.png" alt="#" /></a>
								</div>
							</div>
							{/* <!--/ End Single client -->
							<!-- Single client --> */}
							<div className="single-slider">
								<div className="single-client">
									<a href="#" target="_blank"><img src="/static/img/client/client-3.png" alt="#" /></a>
								</div>
							</div>
							{/* <!--/ End Single client -->
							<!-- Single client --> */}
							<div className="single-slider">
								<div className="single-client">
									<a href="#" target="_blank"><img src="/static/img/client/client-4.png" alt="#" /></a>
								</div>
							</div>
							{/* <!--/ End Single client -->
							<!-- Single client --> */}
							<div className="single-slider">
								<div className="single-client">
									<a href="#" target="_blank"><img src="/static/img/client/client-5.png" alt="#" /></a>
								</div>
							</div>
							{/* <!--/ End Single client -->
							<!-- Single client --> */}
							<div className="single-slider">
								<div className="single-client">
									<a href="#" target="_blank"><img src="/static/img/client/client-6.png" alt="#" /></a>
								</div>
							</div>
							{/* <!--/ End Single client --> */}
						</div>
					</div>
				</div>
			</div>
		</div>

		
			</div>
		)
	}
}

export default Home

